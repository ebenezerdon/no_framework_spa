const Navbar = () => {
  return (`
    <button onclick="_navigateTo('/')">Home</button>
    <button onclick="_navigateTo('about')">About</button>
    <button onclick="_navigateTo('contact')">Contact</button>
  `)
};

export default Navbar;
