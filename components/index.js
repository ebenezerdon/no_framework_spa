export { default as Home } from './Home.js';
export { default as About } from './About.js';
export { default as Contact } from './Contact.js';
export { default as Navbar } from './Navbar.js';
export { default as NotFound } from './NotFound.js';
