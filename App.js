import { Home, About, Contact, Navbar, NotFound } from './components/index.js';

const App = page => {
  const views = {
    ['']: Home(),
    ['/']: Home(),
    about: About(),
    contact: Contact()
  };

  return `
    ${Navbar()}
    ${views[page] || NotFound()}
  `
}

export default App;
