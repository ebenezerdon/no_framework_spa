import App from './App.js';

window._proxyHandler = {
  set(target, key, value) {
    target[key] = value;
    return init();
  },
};

window._globalState = new Proxy({ page: window.location.href.split('/')[3] }, _proxyHandler);
const init = () => document.getElementById('root').innerHTML = App(_globalState.page);

window.onload = init;
window.onpopstate = () => {
  _globalState.page = window.location.href.split('/')[3]
  init()
};

window._navigateTo = route => {
  history.pushState(null, null, route);
  _globalState.page = route;
};
